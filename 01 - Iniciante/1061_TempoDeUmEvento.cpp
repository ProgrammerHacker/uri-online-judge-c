#include <iostream>
#include <string.h>
#include <vector>
#include <stdlib.h>

using namespace std;

void split(string frase, string caractere, vector<string>* saida){
    int valores = frase.find_first_of(caractere);

    while(valores != string::npos){
        if(valores > 0){
        	saida->push_back(frase.substr(0,valores));
        }

        frase = frase.substr(valores+1);
        valores = frase.find_first_of(caractere);
    }

    if(frase.length() > 0){
    	saida->push_back(frase);
    }
}

void tempoDeUmEvento(){

	vector<string> valorD1, valorD2, valorH1, valorH2;
	string d1, d2, h1, h2;

	getline(cin, d1);
	getline(cin, h1);
	getline(cin, d2);
	getline(cin, h2);

	split(d1, " ", &valorD1);
	split(h1, " : ", &valorH1);
	split(d2, " ", &valorD2);
	split(h2, " : ", &valorH2);

	int dia = atoi(valorD2[1].c_str()) - atoi(valorD1[1].c_str());
	int hora = atoi(valorH2[0].c_str()) - atoi(valorH1[0].c_str());
	int minuto = atoi(valorH2[1].c_str()) - atoi(valorH1[1].c_str());
	int segundo = atoi(valorH2[2].c_str()) - atoi(valorH1[2].c_str());

	if (segundo < 0){
	    segundo += 60;
	    minuto--;
	}
	if (minuto < 0){
	    minuto += 60;
	    hora--;
	}
	if (hora < 0){
	    hora += 24;
	    dia--;
	}

	cout << dia << " dia(s)" << endl;
	cout << hora << " hora(s)" << endl;
	cout << minuto << " minuto(s)" << endl;
	cout << segundo << " segundo(s)" << endl;

}
