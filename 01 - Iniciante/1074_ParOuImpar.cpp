#include <iostream>

using namespace std;

void parOuImpar(){

	int n, valor;

	cin >> n;

	for (int i=0; i<n; i++){
	    cin >> valor;
	    if (valor == 0){
	    	cout << "NULL" << endl;
	    }else if (valor % 2 == 0){
	        if (valor > 0){
	            cout << "EVEN POSITIVE" << endl;
	        }else{
	        	cout << "EVEN NEGATIVE" << endl;
	        }
	    }else{
	    	if (valor > 0){
	    		cout << "ODD POSITIVE" << endl;
			}else{
				cout << "ODD NEGATIVE" << endl;
			}
	    }
	}

}
