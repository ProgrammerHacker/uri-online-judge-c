#include <iostream>
#include <iomanip>
#include <math.h>

using namespace std;

void esfera(){

	double volume, pi = 3.14159;
	int raio;

	cin >> raio;

	volume = (4.0/3.0)*pi*pow(raio, 3);

	cout << fixed << setprecision(3);
	cout << "VOLUME = " << volume << endl;

}
