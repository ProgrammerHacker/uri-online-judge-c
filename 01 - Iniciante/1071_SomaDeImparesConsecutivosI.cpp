#include <iostream>

using namespace std;

void somaDeImparesConsecutivosI(){

	int n1, n2, impar = 0, aux;
	bool verdade = true;

	cin >> n1;
	cin >> n2;

	if (n1 > n2){
	    aux = n1;
	    n1 = n2;
	    n2 =aux;
	}

	while (verdade){
	    n1++;
	    if (n1 >= n2){
	        verdade = false;
	    }
	    else if (n1 % 2 != 0){
	        impar = impar + n1;
	    }
	}
	cout << impar << endl;

}
