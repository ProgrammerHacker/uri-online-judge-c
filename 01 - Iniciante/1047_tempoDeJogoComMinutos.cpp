#include <iostream>

using namespace std;

void tempoDeJogoComMinutos(){

	int valor1, valor2, valor3, valor4, hora, minuto;

	cin >> valor1 >> valor2 >> valor3 >> valor4;

	hora = (valor3 - valor1);
	if ((valor3 - valor1) < 0){
	    hora = 24 + (valor3 - valor1);
	}

	minuto = (valor4 - valor2);
	if ((valor4 - valor2) < 0){
	    minuto = 60 + (valor4 - valor2);
	    hora--;
	}

	if (valor3 == valor1 && valor4 == valor2){
	    cout << "O JOGO DUROU 24 HORA(S) E 0 MINUTO(S)" << endl;
	}else{
	    cout << "O JOGO DUROU " << hora << " HORA(S) E "<< minuto << " MINUTO(S)" << endl;
	}

}
