#include <iostream>

using namespace std;

void seisNumerosImpares(){

	int n, cont = 0;
	bool impar = true;

	cin >> n;

	while (impar){
		if (n % 2 != 0){
			cout << n << endl;
			cont = cont +1;
		}
		if (cont == 6){
			impar = false;
		}
		n = n +1;
	}

}
