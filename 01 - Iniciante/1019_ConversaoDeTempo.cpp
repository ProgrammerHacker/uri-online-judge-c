#include <iostream>

using namespace std;

void conversaoDeTempo(){

	int valor, hora, minuto, segundo;

	cin >> valor;

	hora = (valor/60)/60;
	minuto = (valor/60)%60;
	segundo = valor%60;

	cout << hora << ":" << minuto << ":" << segundo << endl;

}
