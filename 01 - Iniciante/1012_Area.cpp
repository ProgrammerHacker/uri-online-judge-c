#include <iostream>
#include <iomanip>

using namespace std;

void area(){

	double valor1, valor2, valor3, triangulo, circulo, trapezio,
	quadrado, retangulo, pi = 3.14159;

	cin >> valor1 >> valor2 >> valor3;

	triangulo = (valor1 * valor3)/2;
	circulo = pi * (valor3 * valor3);
	trapezio = valor3 * (valor1 + valor2)/2;
	quadrado = valor2 * valor2;
	retangulo = valor1 * valor2;

	cout << fixed << setprecision(3);
	cout << "TRIANGULO: " << triangulo << endl;
	cout << "CIRCULO: " << circulo << endl;
	cout << "TRAPEZIO: " << trapezio << endl;
	cout << "QUADRADO: " << quadrado << endl;
	cout << "RETANGULO: " << retangulo << endl;

}
