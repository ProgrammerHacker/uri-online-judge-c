#include <iostream>
#include <iomanip>

using namespace std;

void mediasPonderadas(){

	int n;
	double n1, n2, n3, ponderada;

	cin >> n;

	cout << fixed << setprecision(1);

	while (n > 0){
	    cin >> n1 >> n2 >> n3;
	    ponderada = ((n1 * 2) + (n2 * 3) + (n3 * 5)) / 10;
	    cout << ponderada << endl;
	    n--;
	}

}
