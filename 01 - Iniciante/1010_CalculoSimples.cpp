#include <iostream>
#include <iomanip>

using namespace std;

void calculoSimples(){

	int codigo1, codigo2, quant1, quant2;
	double valorUnitario1, valorUnitario2, valorTotal;

	cin >> codigo1 >> quant1 >> valorUnitario1;
	cin >> codigo2 >> quant2 >> valorUnitario2;

	valorTotal = (quant1*valorUnitario1) + (quant2*valorUnitario2);

	cout << fixed << setprecision(2);
	cout << "VALOR A PAGAR: R$ " << valorTotal << endl;

}
