#include <iostream>
#include <iomanip>

using namespace std;

void media1(){

	double media, valor1, valor2;

	cin >> valor1;
	cin >> valor2;

	media = ((valor1 * 3.5) + (valor2 * 7.5)) / 11;

	cout << fixed << setprecision(5);
	cout << "MEDIA = " << media << endl;

}
