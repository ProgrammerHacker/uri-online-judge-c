#include <iostream>
#include <iomanip>

using namespace std;

void media3(){

	double nota1, nota2, nota3, nota4, media, final, mediaFinal;

	cin >> nota1 >> nota2 >> nota3 >> nota4;

	media = ((nota1 * 2) + (nota2 * 3) + (nota3 * 4) + (nota4 * 1)) / 10.0;

	cout << fixed << setprecision(1);
	cout << "Media: " << media << endl;

	if (media >= 7){
	    cout << "Aluno aprovado." << endl;
	}else if (media < 5){
	    cout << "Aluno reprovado." << endl;
	}else if (media >= 5 && media < 7){
	    cout << "Aluno em exame." << endl;

	    cin >> final;
	    cout << "Nota do exame: " << final << endl;
	    mediaFinal = (media + final) / 2;

	    if (mediaFinal >= 5){
	        cout << "Aluno aprovado." << endl;
	    }else{
	        cout << "Aluno reprovado" << endl;
	    }
	    cout << "Media final: " << mediaFinal << endl;
	}

}
