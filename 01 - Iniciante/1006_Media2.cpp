#include <iostream>
#include <iomanip>

using namespace std;

void media2(){

	double media, nota1, nota2, nota3;

	cin >> nota1;
	cin >> nota2;
	cin >> nota3;

	media = ((nota1 * 2) + (nota2 * 3) + (nota3 * 5)) / 10;

	cout << fixed << setprecision(1);
	cout << "MEDIA = " << media << endl;

}
