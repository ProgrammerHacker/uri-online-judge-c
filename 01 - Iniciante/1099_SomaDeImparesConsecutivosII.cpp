#include <iostream>

using namespace std;

void somaDeImparesConsecutivosII(){

	int n, n1, n2, aux, soma;

	cin >> n;

	for(int i=1; i<=n; i++){

	    cin >> n1 >> n2;

	    if (n1 > n2){
	    	aux = n1;
	        n1= n2;
			n2 = aux;
	    }

	    soma = 0;

	    while (n1 < n2){
	        n1 +=1;
	        if (n1 == n2){
	            break;
	        }else if (n1 % 2 == 1){
	            soma = soma + n1;
	        }
	    }
	    cout << soma << endl;
	}
}
