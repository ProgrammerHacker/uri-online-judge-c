#include <iostream>

using namespace std;

void cedulas(){

	int valor, cem, cinquenta, vinte, dez, cinco, dois, um;

	cin >> valor;

	cem = valor/100;
	cinquenta = (valor%100)/50;
	vinte = ((valor%100)%50)/20;
	dez = (((valor%100)%50)%20)/10;
	cinco = ((((valor%100)%50)%20)%10)/5;
	dois = (((((valor%100)%50)%20)%10)%5)/2;
	um = ((((((valor%100)%50)%20)%10)%5)%2)/1;

	cout << valor << endl;
	cout << cem << " nota(s) de R$ 100,00" << endl;
	cout << cinquenta << " nota(s) de R$ 50,00" << endl;
	cout << vinte << " nota(s) de R$ 20,00" << endl;
	cout << dez << " nota(s) de R$ 10,00" << endl;
	cout << cinco << " nota(s) de R$ 5,00" << endl;
	cout << dois << " nota(s) de R$ 2,00" << endl;
	cout << um << " nota(s) de R$ 1,00" << endl;

}
