#include <iostream>
#include <math.h>

using namespace std;

void quadradoDePares(){

	int cont = 1, valor, res;

	cin >> valor;

	while (cont <= valor){
	    if (cont % 2 == 0){
	    	res = pow(cont, 2);
	        cout << cont << "^2 = " << res << endl;
	    }
	    cont++;
	}
}
