#include <iostream>
#include <iomanip>

using namespace std;

void gastoDeCombustivel(){

	int valor1, valor2;
	double litros;

	cin >> valor1;
	cin >> valor2;

	litros = (valor1 * valor2)/12.0;

	cout << fixed << setprecision(3);
	cout << litros << endl;

}
