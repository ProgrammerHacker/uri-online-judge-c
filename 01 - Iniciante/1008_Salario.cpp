#include <iostream>
#include <iomanip>

using namespace std;

void salario(){

	int numFuncionario, numHoraTrab;
	double salarioo, valorHora;

	cin >> numFuncionario;
	cin >> numHoraTrab;
	cin >> valorHora;

	salarioo = numHoraTrab * valorHora;

	cout << "NUMBER = " << numFuncionario << endl;
	cout << fixed << setprecision(2);
	cout << "SALARY = U$ " << salarioo << endl;

}
