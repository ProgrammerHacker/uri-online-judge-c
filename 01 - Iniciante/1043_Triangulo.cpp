#include <iostream>
#include <iomanip>

using namespace std;

void triangulo(){

	double valor1, valor2, valor3, area, perimetro;

	cin >> valor1 >> valor2 >> valor3;

	cout << fixed << setprecision(1);
	if (valor1 + valor2 > valor3 && valor2 + valor3 > valor1 && valor1 + valor3 > valor2){
	    perimetro = valor1 + valor2 + valor3;
	    cout << "Perimetro = " << perimetro << endl;
	}else{
	    area = (valor3 * (valor1 + valor2)) / 2;
	    cout << "Area = " << area << endl;
	}

}
