#include <iostream>
#include <iomanip>

using namespace std;

void lanche(){

	int codigo, quant;
	double total;

	cin >> codigo >> quant;

	cout << fixed << setprecision(2);
	if (codigo == 1){
	    total = 4.0*quant;
	    cout << "Total: R$ " << total << endl;
	}
	if (codigo == 2){
	    total = 4.5*quant;
	    cout << "Total: R$ " << total << endl;
	}
	if (codigo == 3){
	    total = 5.0*quant;
	    cout << "Total: R$ " << total << endl;
	}
	if (codigo == 4){
	    total = 2.0*quant;
	    cout << "Total: R$ " << total << endl;
	}
	if (codigo == 5){
	    total = 1.5*quant;
	    cout << "Total: R$ " << total << endl;;
	}

}
