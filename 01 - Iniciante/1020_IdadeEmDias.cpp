#include <iostream>>

using namespace std;

void idadeEmDias(){

	int valor, ano, mes, dias;

	cin >> valor;

	ano = int(valor/365);
	mes = int((valor%365)/30);
	dias = (valor-(ano*365))-(mes*30);

	cout << ano << " ano(s)" << endl;
	cout << mes << " mes(es)" << endl;
	cout << dias << " dia(s)" << endl;

}
