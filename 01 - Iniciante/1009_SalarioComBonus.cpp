#include <iostream>
#include <string.h>
#include <iomanip>

using namespace std;

void salarioComBonus(){

	string nome;
	double salarioFinal, salarioFixo, valorVendEfet;

	cin >> nome;
	cin >> salarioFixo;
	cin >> valorVendEfet;

	salarioFinal = salarioFixo + (valorVendEfet * 0.15);

	cout << fixed << setprecision(2);
	cout << "TOTAL = R$ " << salarioFinal << endl;

}
