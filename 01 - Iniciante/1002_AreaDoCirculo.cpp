#include <iostream>
#include <math.h>
#include <iomanip>

using namespace std;

void areaDoCirculo() {

	double pi =  3.14159;
	double raio, area;

	cin >> raio;

	area = pi * pow(raio, 2);

	cout << fixed << setprecision(4);
	cout << "A=" << area << endl;

}
