#include <iostream>

using namespace std;

void tempoDeJogo(){

	int valor1, valor2, hora;

	cin >> valor1 >> valor2;

	hora = valor2 - valor1;

	if ((valor2 - valor1) < 0){
	    hora = 24 + (valor2 - valor1);
	    cout << "O JOGO DUROU " << hora << " HORA(S)" << endl;
	}else if (valor2 == valor1){
	    cout << "O JOGO DUROU 24 HORA(S)" << endl;
	}else{
	    cout << "O JOGO DUROU " << hora << " HORA(S)" << endl;
	}

}
