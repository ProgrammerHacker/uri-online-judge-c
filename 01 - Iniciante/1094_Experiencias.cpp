#include <iostream>
#include <iomanip>

using namespace std;

void experiencias(){

	int n;
	double quant = 0, total = 0, coelho = 0, sapo = 0, rato = 0;
	char tipo;

	cin >> n;

	while (n > 0){
	    cin >> quant >> tipo;

	    if (tipo == 'C'){
	        coelho = coelho + quant;
	    }else if (tipo == 'S'){
	        sapo = sapo + quant;
	    }else if (tipo == 'R'){
	        rato = rato + quant;
	    }

	    total = total + quant;
	    n--;
	}

	cout << "Total: " << total << " cobaias" << endl;
	cout << "Total de coelhos: " << coelho << endl;
	cout << "Total de ratos: " << rato << endl;
	cout << "Total de sapos: " << sapo << endl;

	cout << fixed << setprecision(2);

	cout << "Percentual de coelhos: " << ((coelho / total) * 100) << " %" << endl;
	cout << "Percentual de ratos: " << ((rato / total) * 100) << " %" << endl;
	cout << "Percentual de sapos: " << ((sapo / total) * 100) << " %" << endl;

}
