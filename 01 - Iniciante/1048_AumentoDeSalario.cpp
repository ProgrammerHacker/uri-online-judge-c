#include <iostream>
#include <iomanip>

using namespace std;

void aumentoDeSalario(){

	double salario, salarioFinal;

	cin >> salario;

	cout << fixed << setprecision(2);

	if (salario >= 0 && salario <= 400){
	    salarioFinal = salario + (salario * (15.0 / 100));
	    cout << "Novo salario: " << salarioFinal << endl;
	    cout << "Reajuste ganho: " << (salario * (15.0 / 100)) << endl;
	    cout << "Em percentual: 15 %" << endl;
	}else if (salario > 400 && salario <= 800){
	    salarioFinal = salario + (salario * (12.0 / 100));
	    cout << "Novo salario: " << salarioFinal << endl;
	    cout << "Reajuste ganho: " << (salario * (12.0 / 100)) << endl;
	    cout << "Em percentual: 12 %" << endl;
	}else if (salario > 800 && salario <= 1200){
	    salarioFinal = salario + (salario * (10.0 / 100));
	    cout << "Novo salario: " << salarioFinal << endl;
	    cout << "Reajuste ganho: " << (salario * (10.0 / 100)) << endl;
	    cout << "Em percentual: 10 %" << endl;
	}else if (salario > 1200 && salario <= 2000){
	    salarioFinal = salario + (salario * (7.0 / 100));
	    cout << "Novo salario: " << salarioFinal << endl;
	    cout << "Reajuste ganho: " << (salario * (7.0 / 100)) << endl;
	    cout << "Em percentual: 7 %" << endl;
	}else if (salario > 2000){
	    salarioFinal = salario + (salario * (4.0 / 100));
	    cout << "Novo salario: " << salarioFinal << endl;
	    cout << "Reajuste ganho: " << (salario * (4.0 / 100)) << endl;
	    cout << "Em percentual: 4 %" << endl;
	}

}
