#include <iostream>
#include <iomanip>

using namespace std;

void consumo(){

	int distancia;
	double conbustivel, kml;

	cin >> distancia;
	cin >> conbustivel;

	kml = distancia/conbustivel;

	cout << fixed << setprecision(3);
	cout << kml << " km/l" << endl;

}
