#include <iostream>
#include <string.h>
#include <sstream>

using namespace std;

void sequenciaDeNumerosESoma(){

	int n1, n2, soma, aux;
	string sequencia;

	cin >> n1 >> n2;

	while (n1>0 and n2>0){
	    sequencia = "";
	    soma = 0;
	    if(n1>n2){
	    	aux = n1;
	    	n1 = n2;
	    	n2 = aux;
	    }

	    for (n1; n1<=n2; n1++){
	    	ostringstream convert;
	    	convert << n1;
	        sequencia += convert.str()+" ";
	        soma = soma+n1;
	    }

		cout << sequencia << "Sum=" << soma << endl;
		cin >> n1 >> n2;
	}
}
