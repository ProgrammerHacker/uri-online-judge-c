#include <iostream>
#include <math.h>
#include <iomanip>

using namespace std;

void distanciaEntreDoisPontos(){

	double y1, y2, x1, x2, distancia;

	cin >> x1 >> y1;
	cin >> x2 >> y2;

	distancia = sqrt(pow((x2-x1), 2) + pow((y2-y1), 2));

	cout << fixed << setprecision(4);
	cout << distancia << endl;

}
