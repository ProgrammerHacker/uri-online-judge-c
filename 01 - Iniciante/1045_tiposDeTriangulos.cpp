#include <iostream>
#include <math.h>

using namespace std;

void tiposDeTriangulos(){

	double valor1, valor2, valor3, aux;

	cin >> valor1 >> valor2 >> valor3;

	if (valor1 < valor2){
	    aux = valor1;
	    valor1 = valor2;
	    valor2 = aux;
	}
	if (valor2 < valor3){
	    aux = valor2;
	    valor2 = valor3;
	    valor3 = aux;
	}
	if (valor1 < valor2){
	    aux = valor1;
	    valor1 = valor2;
	    valor2 = aux;
	}

	if (valor1 >= (valor2 + valor3)){
	    cout << "NAO FORMA TRIANGULO" << endl;
	}else{
	    if (pow(valor1, 2) == pow(valor2, 2) + pow(valor3, 2)){
	        cout << "TRIANGULO RETANGULO" << endl;
	    }else if (pow(valor1, 2) > pow(valor2, 2) + pow(valor3, 2)){
	        cout << "TRIANGULO OBTUSANGULO" << endl;
	    }else if (pow(valor1, 2) < pow(valor2, 2) + pow(valor3, 2)){
	        cout << "TRIANGULO ACUTANGULO" << endl;
		}
		if (valor1 == valor2 && valor1 == valor3){
	        cout << "TRIANGULO EQUILATERO" << endl;
		}else if (valor1 == valor2 || valor2 == valor3 || valor3 == valor1){
	        cout << "TRIANGULO ISOSCELES" << endl;
		}
	}

}
