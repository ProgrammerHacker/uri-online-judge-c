#include <iostream>
#include <iomanip>

using namespace std;

void impostoDeRenda(){

	double salario;

	cin >> salario;

	cout << fixed << setprecision(2);

	if (salario <= 2000.0){
	    cout << "Isento" << endl;
	}else if (salario < 3000.0){
	    salario = (salario - 2000.0) * (8.0 / 100);
	    cout << "R$ " << salario << endl;
	}else if (salario < 4500.0){
	    salario = (1000.0 * (8.0 / 100)) + (salario - 3000) * (18.0 / 100);
	    cout << "R$ " << salario << endl;
	}else{
	    salario = (1000.0 * (8.0 / 100)) + (1500.0 * (18.0 / 100)) + (salario - 4500) * (28.0 / 100);
	    cout << "R$ " << salario << endl;
	}

}
